package kakahair;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.kakahair.common.util.Utility;
import com.kakahair.plat.entity.Bservice;
import com.kakahair.plat.entity.Evaluation;
import com.kakahair.plat.entity.Gallery;
import com.kakahair.plat.entity.Technician;
import com.kakahair.plat.mapper.BserviceMapper;
import com.kakahair.plat.mapper.EvaluationMapper;
import com.kakahair.plat.mapper.GalleryMapper;
import com.kakahair.plat.mapper.TechnicianMapper;
import com.kakahair.plat.service.BserviceService;
import com.mysql.jdbc.Blob;

public class CRUDTest {
	// @Test
	public void testEvaInsert() {
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "spring/spring-framework.xml",
				"spring/app-tx.xml", "spring/app-datasource.xml", "spring/app-ehcache.xml" });

	}

	@Test
	public void testGalleryTest() {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				new String[] { "spring/spring-framework.xml", "spring/app-datasource.xml", "spring/app-ehcache.xml" });
		BserviceMapper bservicemapper = context.getBean(BserviceMapper.class);
		TechnicianMapper techmapper = context.getBean(TechnicianMapper.class);
		//BserviceService bserviceService = context.getBean(BserviceService.class);
		List<Bservice> bservices = new ArrayList<Bservice>();
		Bservice bs = new Bservice().setName("洗剪吹").setPrice(BigDecimal.valueOf(15));
		bservices.add(bs);
		bs = new Bservice().setName("洗发").setPrice(BigDecimal.valueOf(15));
		bservices.add(bs);
		bs = new Bservice().setName("护理").setPrice(BigDecimal.valueOf(38)).setMoreChoice(true);
		List<Bservice> mbservices = new ArrayList<Bservice>();
		Bservice mbs = new Bservice().setName("护理1").setPrice(BigDecimal.valueOf(38));
		mbservices.add(mbs);
		mbs = new Bservice().setName("护理2").setPrice(BigDecimal.valueOf(58));
		mbservices.add(mbs);
		mbs = new Bservice().setName("护理3").setPrice(BigDecimal.valueOf(88));
		mbservices.add(mbs);
		bs.setMoreService(mbservices);
		bservices.add(bs);
		bs = new Bservice().setName("烫发").setPrice(BigDecimal.valueOf(88)).setMoreChoice(true);
		 mbservices = new ArrayList<Bservice>();
		mbs = new Bservice().setName("特价").setPrice(BigDecimal.valueOf(88));
		mbservices.add(mbs);
		mbs = new Bservice().setName("优质烫发").setPrice(BigDecimal.valueOf(110));
		mbservices.add(mbs);
		mbs = new Bservice().setName("大众点评").setPrice(BigDecimal.valueOf(150));
		mbservices.add(mbs);
		mbs = new Bservice().setName("店长推荐").setPrice(BigDecimal.valueOf(200));
		mbservices.add(mbs);
		bs.setMoreService(mbservices);
		bservices.add(bs);
		bs = new Bservice().setName("染发").setPrice(BigDecimal.valueOf(80)).setMoreChoice(true);
		 mbservices = new ArrayList<Bservice>();
		mbs = new Bservice().setName("特价").setPrice(BigDecimal.valueOf(80));
		mbservices.add(mbs);
		mbs = new Bservice().setName("优质染发").setPrice(BigDecimal.valueOf(100));
		mbservices.add(mbs);
		mbs = new Bservice().setName("无氨染发").setPrice(BigDecimal.valueOf(130));
		mbservices.add(mbs);
		mbs = new Bservice().setName("酸性染发").setPrice(BigDecimal.valueOf(180));
		mbservices.add(mbs);
		bs.setMoreService(mbservices);
		bservices.add(bs);
		TechnicianMapper techemapper = context.getBean(TechnicianMapper.class);
//		techemapper.selectList(new EntityWrapper<Technician>().where("shopId = 92")).forEach(tech -> {
//			bservicemapper.delete(new EntityWrapper<Bservice>().where("techId = {0}", tech.getId()));
//			
//			
//				Integer technicianId = tech.getId();
//				bservices.forEach(bservice -> {
//
//					if (!("护理 染发 烫发").contains(bservice.getName())) {
//						bservicemapper.insert(bservice.setUuid(Utility.getUUID()).setTechId(technicianId));
//					} else {
//						bservice.setMoreChoice(true);
//						bservicemapper.insert(bservice.setUuid(Utility.getUUID()).setTechId(technicianId));
//						bservice.getMoreService().forEach(moreservice -> {
//							Integer parentId = Integer.valueOf(bservicemapper.selectList(new EntityWrapper<Bservice>()
//									.where("uuid = {0}", bservice.getUuid())).get(0).getId().toString());
//							moreservice.setParentId(parentId);
//							bservicemapper.insert(moreservice.setTechId(technicianId));
//						});
//					}
//			});
//		});
		Technician tech = new Technician().setName("小博老师").setPosition("美发师").setShopId(92).setWorkAge(6).setMinPrice((float) 15);
		techmapper.insert(tech);;
		Integer technicianId =  techmapper
				.selectOne(tech).getId();
		bservices.forEach(bservice -> {
			if (!("护理 染发 烫发").contains(bservice.getName())) {
				bservicemapper.insert(bservice.setUuid(Utility.getUUID()).setTechId(technicianId));
			} else {
				bservice.setMoreChoice(true);
				bservicemapper.insert(bservice.setUuid(Utility.getUUID()).setTechId(technicianId));
				bservice.getMoreService().forEach(moreservice -> {
					Integer parentId = Integer.valueOf(bservicemapper.selectList(new EntityWrapper<Bservice>()
							.where("uuid = {0}", bservice.getUuid())).get(0).getId().toString());
					moreservice.setParentId(parentId);
					bservicemapper.insert(moreservice.setTechId(technicianId));
				});
			}
	});
	}
}
