package com.kakahair.common.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 
 * @ClassName:Utility.java
 * @Description: 工具方法
 * @Author:lxt<839376636@qq.com>
 * @Date:2017年10月6日下午6:45:06
 * @Version:1.1.0
 */
public class Utility {
	public static final String allChar = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static final String letterChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static final String numChar = "0123456789";
	/**
	 * 生成唯一的UUID,主要用户实体类的主键字段
	 * 
	 * @return 唯一的 UUID
	 */
	public static String getUUID() {

		return java.util.UUID.randomUUID().toString().trim().replaceAll("-", "");
	}
	
	/**
	 * 生成当前时间的yyyyMMdd形式的格式时间，用于存储文件的分类
	 * 
	 * @return 当前格式化日期
	 */
	public static String getFormatDate() {
		return (new SimpleDateFormat("yyyyMMdd")).format(new Date());
	}

	/**
	 * 
	 * @Title: getListString
	 * @Description: 将一个list转换成一个字符串，每两个元素之间用逗号隔开
	 * @param list
	 * @return String
	 * 
	 */
	public static String getListString(List<String> list) {
		StringBuffer sbf = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			if (i == (list.size() - 1)) {
				sbf.append(list.get(i).trim());
			} else {
				sbf.append(list.get(i).trim() + ",");
			}
		}
		return sbf.toString();
	}

	/**
	 * 
	 * @Title: getArrayString
	 * @Description: 将一个数组转换成一个字符串，每两个元素之间用逗号隔开
	 * @param array
	 * @return String
	 * 
	 */
	public static String getArrayString(String[] array) {
		StringBuffer sbf = new StringBuffer();
		for (int i = 0; i < array.length; i++) {
			if (i == (array.length - 1)) {
				sbf.append(array[i].trim());
			} else {
				sbf.append(array[i].trim() + ",");
			}
		}
		return sbf.toString();
	}

	/**
	 * 获得一个Long类型的随机数
	 * 
	 * @param min
	 *            最小数
	 * @param max
	 *            最大数
	 * @return 返回一个在介于最大数据和最小数据之间的数据
	 */
	public static long getRandom(int min, int max) {
		return new Random().nextInt(max) + min;
	}

	/**
	 * 返回一个定长的随机纯数字字符串(只包含数字)
	 *
	 * @param length
	 *            随机字符串长度
	 * @return 随机字符串
	 */
	public static String generateNumString(int length) {
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			sb.append(random.nextInt(10));
		}
		return sb.toString();
	}

	/**
	 * 返回一个定长的随机纯字母字符串(只包含大小写字母)
	 *
	 * @param length
	 *            随机字符串长度
	 * @return 随机字符串
	 */
	public static String generateMixString(int length) {
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			sb.append(allChar.charAt(random.nextInt(letterChar.length())));
		}
		return sb.toString();
	}
	/**
	 * 
	 * @Title: getMapInstance
	 * @Description: 创建map对象
	 * @return Map<String,Object>
	 */
	public static Map<String, Object> getMapInstance() {
		return new HashMap<String, Object>();
	}

	/**
	 * 
	 * @Title: getPropertyCollectByReflect
	 * @Description: 通过反射获取某属性的get方法值
	 * @param obj
	 * @param propertyName
	 * @return Object
	 */
	public static Object getPropertyCollectByReflect(Object obj, String propertyName) {
		Class clazz = obj.getClass();
		PropertyDescriptor pd;
		try {
			pd = new PropertyDescriptor(propertyName, clazz);
			if (pd != null) {
				Method getPropertyMethod = pd.getReadMethod();
				Object propertyObj = getPropertyMethod.invoke(obj);
				if (propertyObj != null) {
					return propertyObj;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @author TiddAr
	 * @Title: setPropertyByReflect
	 * @Description:反射set属性
	 * @param @param
	 *            obj
	 * @param @param
	 *            setMethod
	 * @param @param
	 *            value
	 * @param @return
	 *            参数
	 * @return Object 返回类型
	 * @date 2018年4月22日
	 * 
	 */
	public static Object setPropertyByReflect(Object obj, String setMethod, Object value) {
		Class clazz = obj.getClass();
		Method method;
		try {
			method = clazz.getMethod(setMethod, Object.class);
			method.invoke(obj, value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}
	
	public String[] push(String[] src,String[] add) {
		src = src == null?new String[0]:src;
		add = add == null?new String[0]:add;
		String[] newStringArr = new String[src.length+add.length];
		for(int i = 0;i < add.length;i++) {
			newStringArr[src.length+i-1] = add[i];
		}
		return add;
		
	}
	
	  /** 
     * 校验签名 
     */  
    public static boolean checkSignature(String signature, String timestamp, String nonce) {  
        System.out.println("signature:" + signature + "timestamp:" + timestamp + "nonc:" + nonce);  
        String[] arr = new String[] { KakahairConst.token, timestamp, nonce };  
        // 将token、timestamp、nonce三个参数进行字典序排序  
        Arrays.sort(arr);  
        StringBuilder content = new StringBuilder();  
        for (int i = 0; i < arr.length; i++) {  
            content.append(arr[i]);  
        }  
        MessageDigest md = null;  
        String tmpStr = null;  
  
        try {  
            md = MessageDigest.getInstance("SHA-1");  
            // 将三个参数字符串拼接成一个字符串进行sha1加密  
            byte[] digest = md.digest(content.toString().getBytes());  
            tmpStr = byteToStr(digest);  
        } catch (NoSuchAlgorithmException e) {  
            e.printStackTrace();  
        }  
  
        content = null;  
        // 将sha1加密后的字符串可与signature对比，标识该请求来源于微信  
        System.out.println(tmpStr.equals(signature.toUpperCase()));  
        return tmpStr != null ? tmpStr.equals(signature.toUpperCase()) : false;  
    }  
  
    /** 
     * 将字节数组转换为十六进制字符串 
     *  
     * @param byteArray 
     * @return 
     */  
    private static String byteToStr(byte[] byteArray) {  
        String strDigest = "";  
        for (int i = 0; i < byteArray.length; i++) {  
            strDigest += byteToHexStr(byteArray[i]);  
        }  
        return strDigest;  
    }  
  
    /** 
     * 将字节转换为十六进制字符串 
     *  
     * @param mByte 
     * @return 
     */  
    private static String byteToHexStr(byte mByte) {  
        char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };  
        char[] tempArr = new char[2];  
        tempArr[0] = Digit[(mByte >>> 4) & 0X0F];  
        tempArr[1] = Digit[mByte & 0X0F];  
  
        String s = new String(tempArr);  
        return s;  
    }  
	
	
	

}
