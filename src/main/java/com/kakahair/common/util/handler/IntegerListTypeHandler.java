/**
* @Title: IntegerListTypeHandler.java
* @Package com.kakahair.common.util.handler
* @Description: TODO(用一句话描述该文件做什么)
* @author TiddAr
* @date 2018年4月28日
* @version V1.0
*/
package com.kakahair.common.util.handler;

import java.util.List;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

/**
* @ClassName: IntegerListTypeHandler
* @Description: TODO(这里用一句话描述这个类的作用)
* @author TiddAr
* @date 2018年4月28日
*
*/

@MappedTypes(List.class)
@MappedJdbcTypes({JdbcType.VARCHAR})
public class IntegerListTypeHandler extends ListTypeHandler<Integer> {

	/**
	* 创建一个新的实例 IntegerListTypeHandler.
	*
	* @param clazz
	*/
	public IntegerListTypeHandler(Class<Integer> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	/* (非 Javadoc)  
	* <p>Title: parseString</p>  
	* <p>Description: </p>  
	* @param string
	* @return  
	* @see com.kakahair.common.util.handler.ListTypeHandler#parseString(java.lang.String)  
	*/  
	@Override
	Integer parseString(String string) {
		// TODO Auto-generated method stub
		return Integer.valueOf(string);
	}
	
}
