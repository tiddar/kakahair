package com.kakahair.common.util;

import javax.servlet.http.HttpServletRequest;

import com.kakahair.plat.entity.User;

/**
 * 
 *@ClassName:SessionUtil.java
 *@Description:  工具类获取当前用户信息
 *@Author:lxt<839376636@qq.com>
 *@Date:2017年10月9日下午9:57:20
 *@Version:1.1.0
 */
public class SessionUtil {
	/**
	 * 存储在session中的用户key值
	 */
	public static final String  SESSION_USER_KEY = "user";

	/**
	 * 
	 * @Title: setLoginUser 
	 * @Description: 设置登录用户信息 
	 * @param request
	 * @param userKey
	 */
	public static void setLoginUser(HttpServletRequest request,User user) {
		request.getSession().setAttribute(SESSION_USER_KEY,user);
	}

	/**
	 * 
	 * @Title: getLoginUser 
	 * @Description: 获取登录用户信息 
	 * @param request
	 * @param userKey
	 * @return Object
	 */
	public static User getLoginUser(HttpServletRequest request) {
		return (User) request.getSession().getAttribute(SESSION_USER_KEY);
	}
}
