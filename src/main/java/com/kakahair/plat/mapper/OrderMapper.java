package com.kakahair.plat.mapper;

import com.kakahair.plat.entity.Order;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface OrderMapper extends BaseMapper<Order> {

}
