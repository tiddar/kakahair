package com.kakahair.plat.mapper;

import com.kakahair.plat.entity.Work;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-27
 */
public interface WorkMapper extends BaseMapper<Work> {

}
