package com.kakahair.plat.mapper;

import com.kakahair.plat.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface UserMapper extends BaseMapper<User> {

}
