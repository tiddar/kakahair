package com.kakahair.plat.mapper;

import com.kakahair.plat.entity.Gallery;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 图库 Mapper 接口
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface GalleryMapper extends BaseMapper<Gallery> {

}
