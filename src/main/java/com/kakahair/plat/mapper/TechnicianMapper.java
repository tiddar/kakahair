package com.kakahair.plat.mapper;

import com.kakahair.plat.entity.Technician;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 理发师表 Mapper 接口
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface TechnicianMapper extends BaseMapper<Technician> {
	
}
