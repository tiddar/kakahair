package com.kakahair.plat.mapper;

import com.kakahair.plat.entity.Shop;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface ShopMapper extends BaseMapper<Shop> {

}
