package com.kakahair.plat.service;

import com.kakahair.plat.entity.Sb;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Tiddar
 * @since 2018-05-07
 */
public interface SbService extends IService<Sb> {

}
