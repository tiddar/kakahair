package com.kakahair.plat.service;

import com.kakahair.plat.entity.Evaluation;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface EvaluationService extends IService<Evaluation> {
	
	/**
	 * 
	
	 * <p>Title: evaluate</p>  
	
	 * <p>Description: 添加评价</p>  
	
	 * @param evaluation
	 * @return 
	 */
	public boolean evaluate(Evaluation evaluation);
	/**
	 * 
	
	 * <p>Title: listByShopId</p>  
	
	 * <p>Description: 通过shopId获取前三个评论</p>  
	
	 * @param shopId
	 * @return
	 */
	public List<Evaluation> listByShopId(Integer shopId);
	
	
	
	
	
	
	
	/**
	 * 
	
	 * <p>Title: listAllByShopId</p>  
	
	 * <p>Description:通过shopId分页获取全部评论 </p>  
	
	 * @param shopId
	 * @return
	 */
	public List<Evaluation> listAllByShopId(Integer shopId,Integer index);
	
	/**
	 * 
	
	 * <p>Title: listByTechId</p>  
	
	 * <p>Description:根据技师id列出评论 </p>  
	
	 * @param techId
	 * @return
	 */
	public List<Evaluation> listByTechId(Integer techId,Integer pageIndex);
	
}
