package com.kakahair.plat.service;

import com.kakahair.plat.entity.Gallery;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 图库 服务类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface GalleryService extends IService<Gallery> {
	public List<Gallery> listAll();
}
