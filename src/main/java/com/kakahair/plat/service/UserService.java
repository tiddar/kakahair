package com.kakahair.plat.service;

import com.kakahair.plat.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface UserService extends IService<User> {

}
