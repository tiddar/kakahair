package com.kakahair.plat.service;

import com.kakahair.plat.entity.Bservice;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface BserviceService extends IService<Bservice> {
	
	/**
	 * 
	* @author TiddAr
	* @Title: selectByBserviceId
	* @Description: 通过技师id查询服务
	* @param @param id
	* @param @return    参数
	* @return List<Bservice>    返回类型
	* @date 2018年4月28日
	*  
	 */
	public List<Bservice> selectByTechId(Integer techid);
}
