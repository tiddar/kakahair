package com.kakahair.plat.service;

import com.kakahair.plat.entity.Shop;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface ShopService extends IService<Shop> {
	
	/**
	 * 
	* @author TiddAr
	* @Title: list
	* @Description: 罗列商户
	* @param @param cityName
	* @param @param areaName
	* @param @param sortType
	* @param @param currentPage
	* @param @param pageSize
	* @param @return    参数
	* @return Page<Shop>    返回类型
	* @date 2018年4月26日
	*  
	 */
	public Page<Shop> list(String cityName,String areaName,Integer sortType,Integer currentPage,Integer pageSize);
	
	/**
	 * 
	* @author TiddAr
	* @Title: list
	* @Description: 查询商户
	* @param @return    参数
	* @return List<Shop>    返回类型
	* @date 2018年4月22日
	*  
	 */
	public List<Shop> querylist(String queryWord);
	
	
	/**
	 * 
	* @author TiddAr
	* @Title: detail
	* @Description: 通过id查询单个shop的详情
	* @param @param shopId
	* @param @return    参数
	* @return Shop    返回类型
	* @date 2018年4月27日
	*  
	 */
	public Shop detail(Integer shopId);
	
	
	/**
	 * 
	
	 * <p>Title: me</p>  
	
	 * <p>Description:通过商家主userId获取商家信息 </p>  
	
	 * @param userId
	 * @return
	 */
	public Shop me(String userId);
	
	/**
	 * 
	
	 * <p>Title: count</p>  
	
	 * <p>Description:获取今日订单。。。。。。。等数据 </p>  
	
	 * @param id
	 * @return
	 */
	public Shop count(Integer id);
}
