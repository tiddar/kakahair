package com.kakahair.plat.service;

import com.kakahair.plat.entity.Order;
import com.kakahair.plat.entity.User;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.service.IService;
import com.github.binarywang.wxpay.bean.order.WxPayAppOrderResult;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.exception.WxPayException;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface OrderService extends IService<Order> {

	/**
	 * 
	
	 * <p>Title: getOrderNum</p>  
	
	 * <p>Description: </p>  
		在 我的 界面获取订单概要
	 * @param userOpenId
	 * @return
	 */
	public Map<String,Integer> getOrderNum(String userOpenId);
	
	/**
	* @author TiddAr
	* @Title: getOrderList
	* @Description: 根据登录用户以及是否使用，是否评价获取订单列表
	*  
	*/
	public List<Order> getOrderList(String queryCondition,String userOpenId);

	/**  
	
	 * <p>Title: place</p>  
	
	 * <p>Description: </p>  
	
	 * @param request
	 * @param allPrice
	 * @param servicesId
	 * @param technicianId
	 * @param appointDate
	 * @param userId
	 * @param shopId
	 * @return
	 * @throws Exception  
	
	 */  
	Order place(HttpServletRequest request, BigDecimal allPrice, List<String> servicesId, Integer technicianId,
			Date appointDate, String userId, Integer shopId,String phoneNum,String technicianName,String remark) throws Exception;


}
