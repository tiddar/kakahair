package com.kakahair.plat.service;

import com.kakahair.plat.entity.Work;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-27
 */
public interface WorkService extends IService<Work> {

}
