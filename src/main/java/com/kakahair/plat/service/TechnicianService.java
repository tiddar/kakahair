package com.kakahair.plat.service;

import com.kakahair.plat.entity.Technician;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 理发师表 服务类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
public interface TechnicianService extends IService<Technician> {
	/**
	 * 
	* @author TiddAr
	* @Title: selectById
	* @Description: 通过id查询 技师详情
	* @param @param id
	* @param @return    参数
	* @return Technician    返回类型
	* @date 2018年4月27日
	*  
	 */
	public Technician selectByTechId(Integer id);
	/**
	 * 
	* @author TiddAr
	* @Title: selectByShopId
	* @Description: 通过shopid查询 对应技师列表
	* @param @param shopId
	* @param @return    参数
	* @return List<Technician>    返回类型
	* @date 2018年4月27日
	*  
	 */
	public List<Technician> selectByShopId(Integer shopId);
}
