package com.kakahair.plat.service.impl;

import com.kakahair.plat.entity.Bservice;
import com.kakahair.plat.mapper.BserviceMapper;
import com.kakahair.plat.service.BserviceService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
@Service
public class BserviceServiceImpl extends ServiceImpl<BserviceMapper, Bservice> implements BserviceService {

	/**
	 * (非 Javadoc) <p>Title: selectByBserviceId</p> <p>Description: </p>
	 * 
	 * @param techid
	 * 
	 * @return
	 * 
	 * @see com.kakahair.plat.service.BserviceService#selectByBserviceId(java.lang.
	 * Integer)
	 */
	@Override
	public List<Bservice> selectByTechId(Integer techId) {

		 List<Bservice> services = this.selectList(new EntityWrapper<Bservice>().where("techId={0}", techId));
		 services.forEach(bservice -> {
			bservice.setMoreService(bservice.getMoreChoice()?this.selectList(new EntityWrapper<Bservice>().where("parentId={0}", bservice.getId())):null);
		});
		return services;
	}

}
