package com.kakahair.plat.service.impl;

import com.kakahair.plat.entity.Work;
import com.kakahair.plat.mapper.WorkMapper;
import com.kakahair.plat.service.WorkService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-27
 */
@Service
public class WorkServiceImpl extends ServiceImpl<WorkMapper, Work> implements WorkService {

}
