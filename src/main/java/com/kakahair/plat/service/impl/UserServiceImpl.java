package com.kakahair.plat.service.impl;

import com.kakahair.plat.entity.User;
import com.kakahair.plat.mapper.UserMapper;
import com.kakahair.plat.service.UserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
