package com.kakahair.plat.service.impl;

import com.kakahair.plat.entity.Sb;
import com.kakahair.plat.mapper.SbMapper;
import com.kakahair.plat.service.SbService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Tiddar
 * @since 2018-05-07
 */
@Service
public class SbServiceImpl extends ServiceImpl<SbMapper, Sb> implements SbService {

}
