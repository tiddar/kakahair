package com.kakahair.plat.service.impl;

import com.kakahair.plat.entity.Evaluation;
import com.kakahair.plat.entity.Order;
import com.kakahair.plat.mapper.EvaluationMapper;
import com.kakahair.plat.mapper.OrderMapper;
import com.kakahair.plat.service.EvaluationService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
@Service
public class EvaluationServiceImpl extends ServiceImpl<EvaluationMapper, Evaluation> implements EvaluationService {

	@Autowired
	OrderMapper orderMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.kakahair.plat.service.EvaluationService#evaluate(com.kakahair.plat.entity
	 * .Evaluation)
	 */
	@Override
	public boolean evaluate(Evaluation evaluation) {
		orderMapper.updateById(orderMapper.selectById(evaluation.getOrderId()).setEvaluate(true));
		if (this.selectCount(new EntityWrapper<Evaluation>().where("orderId={0}", evaluation.getOrderId())) != 0) {
			return this.update(evaluation,
					new EntityWrapper<Evaluation>().where("orderId={0}", evaluation.getOrderId()));
		} else {
			return this.insert(evaluation);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.kakahair.plat.service.EvaluationService#listByShopId(java.lang.Integer)
	 */
	@Override
	public List<Evaluation> listByShopId(Integer shopId) {
		List<Object> orderIds = orderMapper
				.selectObjs(new EntityWrapper<Order>().where("shopId = {0}", shopId).setSqlSelect("id"));
		orderIds.add(0);
		return this.selectPage(new Page<Evaluation>(0, 3), new EntityWrapper<Evaluation>().in("orderId", orderIds).orderBy("time",false))
				.getRecords();
	}

	/* (non-Javadoc)
	 * @see com.kakahair.plat.service.EvaluationService#listAllByShopId(java.lang.Integer)
	 */
	@Override
	public List<Evaluation> listAllByShopId(Integer shopId,Integer pageIndex) {
		// TODO Auto-generated method stub
		List<Object> orderIds = orderMapper
				.selectObjs(new EntityWrapper<Order>().where("shopId = {0}", shopId).setSqlSelect("id"));

		orderIds.add(0);
		return this.selectPage(new Page<Evaluation>(pageIndex, 6), new EntityWrapper<Evaluation>().in("orderId", orderIds).orderBy("time",false))
				.getRecords();
	}

	/* (non-Javadoc)
	 * @see com.kakahair.plat.service.EvaluationService#listByTechId(java.lang.Integer)
	 */
	@Override
	public List<Evaluation> listByTechId(Integer techId,Integer pageIndex) {
		// TODO Auto-generated method stub
		List<Object> orderIds = orderMapper
				.selectObjs(new EntityWrapper<Order>().where("technicianId = {0}", techId).setSqlSelect("id"));

		orderIds.add(0);
		return this.selectPage(new Page<Evaluation>(pageIndex, 6), new EntityWrapper<Evaluation>().in("orderId", orderIds).orderBy("time",false))
				.getRecords();
	}

}
