package com.kakahair.plat.service.impl;

import com.kakahair.plat.entity.Order;
import com.kakahair.plat.entity.Shop;
import com.kakahair.plat.entity.Technician;
import com.kakahair.plat.entity.User;
import com.kakahair.plat.mapper.ShopMapper;
import com.kakahair.plat.mapper.TechnicianMapper;
import com.kakahair.plat.mapper.UserMapper;
import com.kakahair.plat.service.OrderService;
import com.kakahair.plat.service.ShopService;
import com.kakahair.plat.service.TechnicianService;
import com.kakahair.plat.service.UserService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
@Service
public class ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements ShopService {
	@Autowired
	TechnicianService techService;
	@Autowired
	UserService userService;
	@Autowired
	OrderService orderService;

	public List<Shop> querylist(String queryWord) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Shop> list(String cityName, String areaName, Integer sortType, Integer currentPage, Integer pageSize) {
		Page<Shop> shopPage = null;
		switch (sortType) {
		case 0: {// 离我最近
			shopPage = this.selectPage((Page<Shop>) new Page<Shop>().setCurrent(currentPage).setSize(pageSize),
					new EntityWrapper<Shop>().where("city={0} and area = {1}", cityName, areaName));
			break;
		}
		case 1: {// 人气最高
			shopPage = this.selectPage((Page<Shop>) new Page<Shop>().setCurrent(currentPage).setSize(pageSize),
					new EntityWrapper<Shop>().where("city={0} and area = {1}", cityName, areaName)
							.orderBy("py", false));
			break;
		}
		case 2: {// 折扣最大
			shopPage = this.selectPage((Page<Shop>) new Page<Shop>().setCurrent(currentPage).setSize(pageSize),
					new EntityWrapper<Shop>().where("city={0} and area = {1}", cityName, areaName).orderBy("minPrice",
							true));
			break;
		}
		default: {// 均价最低
			shopPage = this.selectPage((Page<Shop>) new Page<Shop>().setCurrent(currentPage).setSize(pageSize),
					new EntityWrapper<Shop>().where("city={0} and area = {1}", cityName, areaName).orderBy("avePrice",
							true));
			break;
		}
		}
		return shopPage;
	}

	/**
	 * <p>
	 * Title: detail
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param shopId
	 * 
	 * @return
	 * 
	 * @see com.kakahair.plat.service.ShopService#detail(java.lang.Integer)
	 */
	@Override
	public Shop detail(Integer shopId) {
		return selectById(shopId).setTechnicians(techService.selectByShopId(shopId));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kakahair.plat.service.ShopService#me(java.lang.String)
	 */
	@Override
	public Shop me(String userId) {
		// TODO Auto-generated method stub
		Integer shopId = (Integer) userService
				.selectObj(new EntityWrapper<User>().where("openid = {0}", userId).setSqlSelect("shopId"));
		return detail(shopId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kakahair.plat.service.ShopService#count(java.lang.Integer)
	 */
	@Override
	public Shop count(Integer id) {
		Shop shop = this.selectById(id);
		Integer Torder = 0, Aorder = 0, todo = 0;
		BigDecimal Tturnover = new BigDecimal(0), Aturnover = new BigDecimal(0);
		List<Order> orders = new ArrayList<Order>();
		for (Order order : orderService.selectList(
				new EntityWrapper<Order>().where("to_days(payTime) = to_days({0}) and hasDelete = 0", new Date()).and("shopId = {0}", id))) {
			Torder++;
			Tturnover = Tturnover.add(order.getAllPrice());
		}
		for (Order order : orderService.selectList(new EntityWrapper<Order>().where("hasDelete=0").and("shopId = {0}", id))) {
			Aorder++;
			Aturnover = Aturnover.add(order.getAllPrice());
			if(!order.getUsed()) {
				todo ++;
			}
		}
		
		this.updateById(shop.setAorder(Aorder).setAturnover(Aturnover).setTorder(Torder).setTturnover(Tturnover).setTodo(todo));
		return shop;
	}

}
