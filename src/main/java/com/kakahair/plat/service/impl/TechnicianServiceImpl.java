package com.kakahair.plat.service.impl;

import com.kakahair.plat.entity.Bservice;
import com.kakahair.plat.entity.Technician;
import com.kakahair.plat.entity.Work;
import com.kakahair.plat.mapper.BserviceMapper;
import com.kakahair.plat.mapper.TechnicianMapper;
import com.kakahair.plat.mapper.WorkMapper;
import com.kakahair.plat.service.BserviceService;
import com.kakahair.plat.service.TechnicianService;

import ch.qos.logback.classic.Logger;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 理发师表 服务实现类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
@Service
public class TechnicianServiceImpl extends ServiceImpl<TechnicianMapper, Technician> implements TechnicianService {

	@Autowired
	WorkMapper workMapper;
	@Autowired
	BserviceService bservice;

	/**
	 * <p>
	 * Title: selectById
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param id
	 * @return
	 * @see com.kakahair.plat.service.TechnicianService#selectById(java.lang.Integer)
	 */
	@Override
	public Technician selectByTechId(Integer id) {
		System.err.println("id::::::::::"+id);
		System.err.println("tech::::::::"+selectById(id));
		return selectById(id).setWorks(workMapper.selectList(new EntityWrapper<Work>().where("techId={0}", id)))
				.setService(bservice.selectByTechId(id));	
	}

	/**(非 Javadoc)  
	* <p>Title: selectByShopId</p>  
	* <p>Description: </p>  
	* @param shopId
	* @return  
	* @see com.kakahair.plat.service.TechnicianService#selectByShopId(java.lang.Integer)  
	*/  
	@Override
	public List<Technician> selectByShopId(Integer shopId) {
		List<Technician> techs = new ArrayList<Technician>();
		 this.selectList(new EntityWrapper<Technician>().setSqlSelect("id").where("shopId={0}", shopId)).forEach(tech ->{
			 techs.add(selectByTechId(tech.getId()));
		 });
		 return techs;
		
	}
}
