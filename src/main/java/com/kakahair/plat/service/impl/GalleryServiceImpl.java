package com.kakahair.plat.service.impl;

import com.kakahair.plat.entity.Gallery;
import com.kakahair.plat.mapper.GalleryMapper;
import com.kakahair.plat.service.GalleryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 图库 服务实现类
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
@Service
public class GalleryServiceImpl extends ServiceImpl<GalleryMapper, Gallery> implements GalleryService {

	public List<Gallery> listAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
