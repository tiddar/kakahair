package com.kakahair.plat.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import com.kakahair.common.controller.BaseController;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 理发师表 前端控制器
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
@Controller
@RequestMapping("/technician")
public class TechnicianController  extends BaseController{

}

