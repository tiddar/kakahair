package com.kakahair.plat.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kakahair.common.controller.BaseController;
import com.kakahair.common.util.AjaxResponse;
import com.kakahair.plat.service.GalleryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 图库 前端控制器
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
@Controller
@RequestMapping("/gallery")
public class GalleryController  extends BaseController{
	
	@Autowired
	GalleryService galleryService;
	
	@ResponseBody
	@RequestMapping("/list")
	public AjaxResponse list() {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			ajaxResponse.setData(galleryService.selectList(null));
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			// TODO: handle exception
			ajaxResponse.setMessage(e.toString());
			ajaxResponse.setSuccess(false);
		}
		
		return ajaxResponse;
	}
	
}

