package com.kakahair.plat.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.kakahair.common.controller.BaseController;
import com.kakahair.common.util.AjaxResponse;
import com.kakahair.plat.entity.Bservice;
import com.kakahair.plat.entity.Order;
import com.kakahair.plat.service.BserviceService;
import com.kakahair.plat.service.OrderService;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */

@RestController
@RequestMapping("/bservice")
public class BserviceController extends BaseController {
	@Autowired
	BserviceService bservice;
	@Autowired
	OrderService oservice;

	@ResponseBody
	@GetMapping("getnames/{orderId}")
	public List<String> detail(@PathVariable String orderId) {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		List<String> servicenames = new ArrayList<String>();
		bservice.selectList(new EntityWrapper<Bservice>().in("id",
				oservice.selectOne((new EntityWrapper<Order>().where("id = {0}", orderId))).getServicesId()))
				.forEach(service -> {
					servicenames.add(service.getName());
				});
		return servicenames;

	}
}
