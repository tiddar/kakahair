package com.kakahair.plat.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.kakahair.common.controller.BaseController;
import com.kakahair.common.util.AjaxResponse;
import com.kakahair.common.util.SessionUtil;
import com.kakahair.common.util.Utility;
import com.kakahair.plat.entity.Bservice;
import com.kakahair.plat.entity.Order;
import com.kakahair.plat.entity.Shop;
import com.kakahair.plat.entity.User;
import com.kakahair.plat.service.OrderService;
import com.kakahair.plat.service.ShopService;

import net.sf.json.JSONObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
@Controller
@RequestMapping("/order")
public class OrderController extends BaseController {
	@Autowired
	OrderService orderService;
	@Autowired
	ShopService shopService;

	@ResponseBody
	@PostMapping("/place")
	public AjaxResponse make(@RequestBody JSONObject orderData) {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			String remark = orderData.getString("remark");
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date appointDate = sdfDate
					.parse(orderData.getString("appointDate") + " " + orderData.getString("appointTime"));
			Integer technicianId = orderData.optInt("technicianId", 0);// ZWC is DaShaZi
			String technicianName = orderData.getString("technicianName");
			Integer shopId = orderData.optInt("shopId", 0);// ZWC is DaShaZi
			String phoneNum = orderData.getString("phoneNum");
			BigDecimal allPrice = BigDecimal.valueOf(orderData.getDouble("allPrice"));
			List<String> servicesId = new ArrayList<String>();
			for (Object bservice : orderData.getJSONArray("service")) {
				JSONObject tservice = (JSONObject) bservice;
				if (tservice.getBoolean("choosed")) {
					servicesId.add(tservice.getString("id"));
				}
			} // ZWC is DaShaZi
			ajaxResponse.ok(
					orderService.place(request, allPrice, servicesId, technicianId, appointDate,
							SessionUtil.getLoginUser(request).getOpenid(), shopId, phoneNum, technicianName, remark),
					"下单成功");
		} catch (Exception e) {
			e.printStackTrace();
			ajaxResponse.setMessage(e.toString());
			ajaxResponse.setSuccess(false);
		}
		return ajaxResponse;

	}

	
	@ResponseBody
	@GetMapping("/orderNum")
	public AjaxResponse orderNum() {
		AjaxResponse res = AjaxResponse.instance();
		try {
			User loginUser = (User) SessionUtil.getLoginUser(request);
			res.setData(orderService.getOrderNum(loginUser.getOpenid()));
			res.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			res.setSuccess(false);
			res.setMessage(e.toString());
		}
		return res;
	}

	@ResponseBody
	@GetMapping("/orderListAll")
	public AjaxResponse listAll() {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			String userId = ((User) SessionUtil.getLoginUser(request)).getOpenid();
			List<Order> orders = orderService.getOrderList("", userId);

			ajaxResponse.setData(orders);
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxResponse.setMessage(e.toString());
			ajaxResponse.setSuccess(false);
		}
		return ajaxResponse;
	}

	@ResponseBody
	@GetMapping("/orderListUse")
	public AjaxResponse listUse() {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			String userId = ((User) SessionUtil.getLoginUser(request)).getOpenid();
			List<Order> orders = orderService.getOrderList("used=0", userId);

			ajaxResponse.setData(orders);
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxResponse.setMessage(e.toString());
			ajaxResponse.setSuccess(false);
		}
		return ajaxResponse;
	}

	@ResponseBody
	@GetMapping("/orderListEva")
	public AjaxResponse listEva() {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			String userId = ((User) SessionUtil.getLoginUser(request)).getOpenid();
			List<Order> orders = orderService.getOrderList("used=1 and evaluate=0", userId);

			ajaxResponse.setData(orders);
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxResponse.setMessage(e.toString());
			ajaxResponse.setSuccess(false);
		}
		return ajaxResponse;
	}

	@ResponseBody
	@GetMapping("/shopAll/{shopId}")
	public AjaxResponse shopAll(@PathVariable Integer shopId) {

		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			ajaxResponse.setData(
					orderService.selectList(new EntityWrapper<Order>().and("hasDelete=0").and("shopId={0}", shopId)));
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			// TODO: handle exception
			ajaxResponse.setSuccess(false);
			ajaxResponse.setMessage("订单查询失败" + e.getMessage());
		}
		return ajaxResponse;

	}

	@ResponseBody
	@GetMapping("/todayAll/{shopId}")
	public AjaxResponse todayAll(@PathVariable Integer shopId) {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			ajaxResponse.setData(orderService
					.selectList(new EntityWrapper<Order>().where("to_days(appointDate) = to_days({0})", new Date())
							.and("hasDelete=0").and("shopId={0}", shopId)));
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			// TODO: handle exception
			ajaxResponse.setSuccess(false);
			ajaxResponse.setMessage("订单查询失败" + e.getMessage());
		}
		return ajaxResponse;

	}

	@ResponseBody
	@GetMapping("/todayNo/{shopId}")
	public AjaxResponse todayNo(@PathVariable Integer shopId) {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			ajaxResponse.setData(orderService
					.selectList(new EntityWrapper<Order>().where("to_days(appointDate) = to_days({0})", new Date())
							.and("used=false").and("hasDelete=0").and("shopId={0}", shopId)));
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			// TODO: handle exception
			ajaxResponse.setSuccess(false);
			ajaxResponse.setMessage("订单查询失败" + e.getMessage());
		}
		return ajaxResponse;

	}
	@ResponseBody
	@GetMapping("/allNo/{shopId}")
	public AjaxResponse allNo(@PathVariable Integer shopId) {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			ajaxResponse.setData(orderService
					.selectList(new EntityWrapper<Order>()
							.where("used=false").and("hasDelete=0").and("shopId={0}", shopId)));
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			// TODO: handle exception
			ajaxResponse.setSuccess(false);
			ajaxResponse.setMessage("订单查询失败" + e.getMessage());
		}
		return ajaxResponse;

	}

	@ResponseBody
	@PostMapping("/use")
	public AjaxResponse use(@RequestBody JSONObject data) {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			String orderId = data.getString("orderId");
			Order order = orderService.selectById(orderId).setUsed(true);
			orderService.updateById(order);
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			ajaxResponse.setSuccess(false);
		}
		return ajaxResponse;
	}

	@ResponseBody
	@DeleteMapping("/cancel")
	public AjaxResponse cancelS(String orderId) {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			Order order = orderService.selectById(orderId).setHasDelete(true);
			orderService.updateById(order);
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxResponse.setSuccess(false);
		}
		return ajaxResponse;
	}

	@ResponseBody
	@PostMapping("/pay")
	public AjaxResponse pay(@RequestBody JSONObject data) throws ParseException {
		AjaxResponse ajaxResponse = AjaxResponse.instance();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String payType = data.getString("payType");
			String orderId = data.getString("orderId");
			Date payTime = sdf.parse(data.getString("payTime"));
			Order order = orderService.selectById(orderId).setPayTime(payTime).setPayType(payType);
			orderService.updateById(order.setHasDelete(false));
			ajaxResponse.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxResponse.setSuccess(false);
			ajaxResponse.setMessage(e.getMessage());
		}
		return ajaxResponse;
	}
	
	
	
}
