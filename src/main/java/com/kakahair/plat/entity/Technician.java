package com.kakahair.plat.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 理发师表
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */

@TableName("t_technician")
public class Technician implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value="id",type=IdType.AUTO)
    private Integer id;
    /**
     * 理发师照片
     */
    private String pic;
    /**
     * 理发师姓名
     */
    private String name;
    /**
     * 理发师职位
     */
    private String position;
    /**
     * 理发师工作时长
     */
    @TableField("workAge")
    private Integer workAge;
    /**
     * 理发师历史评分,使用','隔开
     */
    private List<String> score;
    
    
    private Float minPrice;
    /**
     * 理发师简介
     */
    private String introduction;
    /**
     * 每周休息日，使用','隔开
     */
    @TableField("restTime")
    private List<String>  restTime;
    /**
     * 对应理发店Id
     */
    @TableField("shopId")
    private Integer shopId;

    @TableField(exist = false) 
    private List<Bservice> service;

    @TableField(exist = false) 
    private List<Work> works;
    
    public Integer getId() {
        return id;
    }

    public Technician setId(Integer id) {
        this.id = id;
		return this;
    }

    public String getPic() {
        return pic;
    }

    public Technician setPic(String pic) {
        this.pic = pic;
		return this;
    }

    public String getName() {
        return name;
    }

    public Technician setName(String name) {
        this.name = name;
		return this;
    }

    public String getPosition() {
        return position;
    }

    public Technician setPosition(String position) {
        this.position = position;
		return this;
    }

    public Integer getWorkAge() {
        return workAge;
    }

    public Technician setWorkAge(Integer workAge) {
        this.workAge = workAge;
		return this;
    }

    public List<String> getScore() {
        return score;
    }

    public Technician setScore(List<String> score) {
        this.score = score;
		return this;
    }

    public String getIntroduction() {
        return introduction;
    }

    public Technician setIntroduction(String introduction) {
        this.introduction = introduction;
		return this;
    }

    public List<String> getRestTime() {
        return restTime;
    }

    public Technician setRestTime(List<String> restTime) {
        this.restTime = restTime;
		return this;
    }

    public Integer getShopId() {
        return shopId;
    }

    public Technician setShopId(Integer shopId) {
        this.shopId = shopId;
		return this;
    }

    
    public List<Bservice> getService() {
		return service;
	}

	public List<Work> getWorks() {
		return works;
	}

	public Technician setService(List<Bservice> service) {
		this.service = service;
		return this;
	}

	public Technician setWorks(List<Work> works) {
		this.works = works;
		return this;
	}

	
	
	public Float getMinPrice() {
		return minPrice;
	}

	public Technician setMinPrice(Float minPrice) {
		this.minPrice = minPrice;
		return this;
	}

	@Override
    public String toString() {
        return "Technician{" +
        "id=" + id +
        ", pic=" + pic +
        ", name=" + name +
        ", position=" + position +
        ", workAge=" + workAge +
        ", score=" + score +
        ", introduction=" + introduction +
        ", restTime=" + restTime +
        ", shopId=" + shopId +
        "}";
    }
}
