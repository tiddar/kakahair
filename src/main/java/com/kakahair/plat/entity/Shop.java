package com.kakahair.plat.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-24
 */

@TableName("t_shop")
public class Shop implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商铺名
     */
    private String title;
    /**
     * 商铺店面照片
     */
    private String image;
    /**
     * 最低价
     */
    @TableField("minPrice")
    private BigDecimal minPrice;
    /**
     * 平均消费
     */
    @TableField("avePrice")
    private BigDecimal avePrice;
    /**
     * 详细位置/***第几楼，第几层
     */
    private String local;
    /**
     * 预约电话
     */
    private String phone;
    /**
     * 模糊位置
     */
    private String address;
    /**
     *  平均星级
     */
    private BigDecimal star;
    /**
     * 距离
     */
    private String position;
    /**
     * 商铺头像

     */
    private String avatar;
    private String city;
    private String area;
    @TableField(exist = false) 
    private List<Technician> technicians;
    @TableField(exist = false) 
    private List<Evaluation> evaluations;
    private Integer Aorder;
    private BigDecimal Aturnover;
    private Integer todo;
    private Integer Torder;
    private BigDecimal Tturnover;
    private String openid;
    private String masterId;
    private Integer py;
    public Integer getId() {
        return id;
    }

    public Shop setId(Integer id) {
        this.id = id;
		return this;
    }

    public String getTitle() {
        return title;
    }

    public Shop setTitle(String title) {
        this.title = title;
		return this;
    }

    public String getImage() {
        return image;
    }

    public Shop setImage(String image) {
        this.image = image;
		return this;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public Shop setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
		return this;
    }

    public BigDecimal getAvePrice() {
        return avePrice;
    }

    public Shop setAvePrice(BigDecimal avePrice) {
        this.avePrice = avePrice;
		return this;
    }

    public String getLocal() {
        return local;
    }

    public Shop setLocal(String local) {
        this.local = local;
		return this;
    }

    public String getPhone() {
        return phone;
    }

    public Shop setPhone(String phone) {
        this.phone = phone;
		return this;
    }

    public String getAddress() {
        return address;
    }

    public Shop setAddress(String address) {
        this.address = address;
		return this;
    }

    public BigDecimal getStar() {
        return star;
    }

    public Shop setStar(BigDecimal star) {
        this.star = star;
		return this;
    }

    public String getPosition() {
        return position;
    }

    public Shop setPosition(String position) {
        this.position = position;
		return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public Shop setAvatar(String avatar) {
        this.avatar = avatar;
		return this;
    }

    public String getCity() {
        return city;
    }

    public Shop setCity(String city) {
        this.city = city;
		return this;
    }

    public String getArea() {
        return area;
    }

    public Shop setArea(String area) {
        this.area = area;
		return this;
    }
    
    

    public List<Technician> getTechnicians() {
		return technicians;
	}

	public Shop setTechnicians(List<Technician> technicians) {
		this.technicians = technicians;
		return this;
	}

	public List<Evaluation> getEvaluations() {
		return evaluations;
	}

	public Shop setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
		return this;
	}
	
	

	public Integer getAorder() {
		return Aorder;
	}

	public BigDecimal getAturnover() {
		return Aturnover;
	}

	public Integer getTodo() {
		return todo;
	}

	public Integer getTorder() {
		return Torder;
	}

	public BigDecimal getTturnover() {
		return Tturnover;
	}

	public Shop setAorder(Integer aorder) {
		Aorder = aorder;
		return this;
	}

	public Shop setAturnover(BigDecimal aturnover) {
		Aturnover = aturnover;
		return this;
	}

	public Shop setTodo(Integer todo) {
		this.todo = todo;
		return this;
	}

	public Shop setTorder(Integer torder) {
		Torder = torder;
		return this;
	}

	public Shop setTturnover(BigDecimal tturnover) {
		Tturnover = tturnover;
		return this;
	}
	
	

	public String getOpenid() {
		return openid;
	}

	public Shop setOpenid(String openid) {
		this.openid = openid;
		return this;
	}
	

	public Integer getPy() {
		return py;
	}

	public void setPy(Integer py) {
		this.py = py;
	}
	
	

	public String getMasterId() {
		return masterId;
	}

	public Shop setMasterId(String masterId) {
		this.masterId = masterId;
		return null;
	}

	@Override
    public String toString() {
        return "Shop{" +
        "id=" + id +
        ", title=" + title +
        ", image=" + image +
        ", minPrice=" + minPrice +
        ", avePrice=" + avePrice +
        ", local=" + local +
        ", phone=" + phone +
        ", address=" + address +
        ", star=" + star +
        ", position=" + position +
        ", avatar=" + avatar +
        ", city=" + city +
        ", area=" + area +
        "}";
    }
}
