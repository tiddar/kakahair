package com.kakahair.plat.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */
@TableName("t_order")
public class Order implements Serializable {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.INPUT)
	private String id;
	private Boolean evaluate;
	private Boolean used = false;
	private String img;
	private String title;
	@TableField("payType")
	private String payType;
	@TableField("payTime")
	private Date payTime;
	@TableField("allPrice")
	private BigDecimal allPrice;
	@TableField("appointDate")
	private Date appointDate;
	@TableField("userId")
	private String userId;
	@TableField("technicianId")
	private Integer technicianId;
	@TableField("shopId")
	private Integer shopId;
	private Boolean hasDelete = false;
	@TableField(exist = false)
	private List<Bservice> service;
	private String[] servicesId;
	private String phoneNum;
	@TableField(exist = false)
	private WxPayMpOrderResult wxOrder;
	private String technicianName;
	private String remark;

	private String shopUserId;
	public String getId() {
		return id;
	}

	public Boolean getHasDelete() {
		return hasDelete;
	}

	public Order setId(String id) {
		this.id = id;
		return this;
	}

	public Order setHasDelete(Boolean hasDelete) {
		this.hasDelete = hasDelete;
		return this;
	}

	public Boolean getEvaluate() {
		return evaluate;
	}

	public Order setEvaluate(Boolean evaluate) {
		this.evaluate = evaluate;
		return this;
	}

	public Boolean getUsed() {
		return used;
	}

	public Order setUsed(Boolean used) {
		this.used = used;
		return this;
	}

	public String getImg() {
		return img;
	}

	public Order setImg(String img) {
		this.img = img;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public Order setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getPayType() {
		return payType;
	}

	public Order setPayType(String payType) {
		this.payType = payType;
		return this;
	}

	public Date getPayTime() {
		return payTime;
	}

	public Order setPayTime(Date payTime) {
		this.payTime = payTime;
		return this;
	}

	public BigDecimal getAllPrice() {
		return allPrice;
	}

	public Order setAllPrice(BigDecimal allPrice) {
		this.allPrice = allPrice;
		return this;
	}

	public Date getAppointDate() {
		return appointDate;
	}

	public Order setAppointDate(Date appointDate) {
		this.appointDate = appointDate;
		return this;
	}

	public String getUserId() {
		return userId;
	}

	public Order setUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public Integer getShopId() {
		return shopId;
	}

	public Order setShopId(Integer shopId) {
		this.shopId = shopId;
		return this;
	}

	public List<Bservice> getService() {
		return service;
	}

	public Order setService(List<Bservice> service) {
		this.service = service;
		return this;
	}

	public Integer getTechnicianId() {
		return technicianId;
	}

	public Order setTechnicianId(Integer technicianId) {
		this.technicianId = technicianId;
		return this;
	}

	public String[] getServicesId() {
		return servicesId;
	}

	public Order setServicesId(String[] servicesId) {
		this.servicesId = servicesId;
		return this;
	}
	
	

	public String getPhoneNum() {
		return phoneNum;
	}

	public Order setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
		return this;
	}

	
	public WxPayMpOrderResult getWxOrder() {
		return wxOrder;
	}

	public Order setWxOrder(WxPayMpOrderResult wxOrder) {
		this.wxOrder = wxOrder;
		return this;
	}
	

	public String getTechnicianName() {
		return technicianName;
	}

	public Order setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
		return this;
	}
	
	

	public String getRemark() {
		return remark;
	}

	public Order setRemark(String remark) {
		this.remark = remark;
		return this;
	}

	
	public String getShopUserId() {
		return shopUserId;
	}

	public Order setShopUserId(String shopUserId) {
		this.shopUserId = shopUserId;
		return this;
	}

	@Override
	public String toString() {
		return "Order{" + "id=" + id + ", evaluate=" + evaluate + ", used=" + used + ", img=" + img + ", title=" + title
				+ ", payType=" + payType + ", payTime=" + payTime + ", allPrice=" + allPrice + ", appointDate="
				+ appointDate + ", userId=" + userId + ", techId=" + technicianId + ", shopId=" + shopId + "}";
	}
}
