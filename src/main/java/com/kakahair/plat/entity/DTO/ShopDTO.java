/**
 * 
 */
package com.kakahair.plat.entity.DTO;

import java.util.List;

import com.kakahair.plat.entity.Bservice;
import com.kakahair.plat.entity.Shop;
import com.kakahair.plat.entity.Technician;

/**
 * @author TiddAr
 *
 */
public class ShopDTO {
	private Shop shop;
	private List<Bservice> bservice;
	private List<Technician> technician;
	public Shop getShop() {
		return shop;
	}
	public List<Bservice> getBservice() {
		return bservice;
	}
	public List<Technician> getTechnician() {
		return technician;
	}
	public ShopDTO setShop(Shop shop) {
		this.shop = shop;
		return this;
	}
	public ShopDTO setBservice(List<Bservice> bservice) {
		this.bservice = bservice;
		return this;
	}
	public ShopDTO  setTechnician(List<Technician> technician) {
		this.technician = technician;
		return this;
	}
	@Override
	public String toString() {
		return "ShopDTO [shop=" + shop + ", bservice=" + bservice + ", technician=" + technician + ", getShop()="
				+ getShop() + ", getBservice()=" + getBservice() + ", getTechnician()=" + getTechnician() + "]";
	}
	
	
}
