package com.kakahair.plat.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-23
 */

@TableName("t_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    private String openid;
    /**
     * 头像URL
     */
    @TableField("avatarUrl")
    private String avatarUrl;
    private String city;
    private String country;
    /**
     * 性别
     */
    private Integer gender;
    private String language;
    @TableField("nickName")
    private String nickName;
    private String province;
    private Date lastLoginTime;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
    

    public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public User setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
		return this;
	}

	@Override
    public String toString() {
        return "User{" +
        "openid=" + openid +
        ", avatarUrl=" + avatarUrl +
        ", city=" + city +
        ", country=" + country +
        ", gender=" + gender +
        ", language=" + language +
        ", nickName=" + nickName +
        ", province=" + province +
        "}";
    }
}
