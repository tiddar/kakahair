package com.kakahair.plat.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */

@TableName("t_evaluation")
public class Evaluation implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 星级
     */
    private Integer key;
    /**
     * 图片URI
     */
    private String[] pics;
    /**
     * 其他建议
     */
    private String add;
    /**
     * 对应订单Id
     */
    @TableId(value = "orderId", type = IdType.INPUT)
    private String orderId;
    private Date time;
    private Integer technicianId;
    public Integer getId() {
        return id;
    }

    public Evaluation setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getKey() {
        return key;
    }

    public Evaluation setKey(Integer key) {
        this.key = key;
        return this;
    }

    public String[] getPics() {
        return pics;
    }

    public Evaluation setPics(String[] pics) {
        this.pics = pics;
        return this;
    }

    public String getAdd() {
        return add;
    }

    public Evaluation setAdd(String add) {
        this.add = add;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public Evaluation setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }
    
    

    public Date getTime() {
		return time;
	}

	public Evaluation setTime(Date time) {
		this.time = time;
		return this;
	}
	
	

	public Integer getTechnicianId() {
		return technicianId;
	}

	public Evaluation setTechnicianId(Integer technicianId) {
		this.technicianId = technicianId;
		return this;
	}

	@Override
    public String toString() {
        return "Evaluation{" +
        "id=" + id +
        ", key=" + key +
        ", pics=" + pics +
        ", add=" + add +
        ", orderId=" + orderId +
        "}";
    }
}
