package com.kakahair.plat.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 
 * </p>
 *
 * @author Tiddar
 * @since 2018-05-07
 */
public class Sb implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value="id",type = IdType.UUID)
    private String id;
    private String content;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public Sb setContent(String content) {
        this.content = content;
        return this;
    }

    @Override
    public String toString() {
        return "Sb{" +
        "id=" + id +
        ", content=" + content +
        "}";
    }
}
