package com.kakahair.plat.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */

@TableName("t_bservice")
public class Bservice implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value="id",type=IdType.AUTO)
    private Integer id;
    /**
     * 服务名('烫发','洗剪吹'...)
     */
    private String name;
    /**
     * 价钱
     */
    private BigDecimal price;
    /**
     * 对应理发师Id
     */
    @TableField("techId")
    private Integer techId;
    private Boolean moreChoice = false;

    @TableField(exist = false) 
    private List<Bservice> moreService;
    
    private String uuid;
   
    
    private Integer parentId;

    public Integer getId() {
        return id;
    }

    public Bservice setId(Integer id) {
        this.id = id;
		return this;
    }

    public String getName() {
        return name;
    }

    public Bservice setName(String name) {
        this.name = name;
		return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Bservice setPrice(BigDecimal price) {
        this.price = price;
		return this;
    }

    public Integer getTechId() {
        return techId;
    }

    public Bservice setTechId(Integer techId) {
        this.techId = techId;
		return this;
    }
    

    public Boolean getMoreChoice() {
		return moreChoice;
	}

	public List<Bservice> getMoreService() {
		return moreService;
	}

	public Bservice setMoreChoice(Boolean moreChoice) {
		this.moreChoice = moreChoice;
		return this;
	}

	public Bservice setMoreService(List<Bservice> moreService) {
		this.moreService = moreService;
		return this;
	}
	
	public Integer getParentId() {
		return parentId;
	}

	public Bservice setParentId(Integer parentId) {
		this.parentId = parentId;
		return this;
	}

	
	public String getUuid() {
		return uuid;
	}

	public Bservice setUuid(String uuid) {
		this.uuid = uuid;
		return this;
	}

	@Override
    public String toString() {
        return "Bservice{" +
        "id=" + id +
        ", name=" + name +
        ", price=" + price +
        ", techId=" + techId +
        "}";
    }
}
