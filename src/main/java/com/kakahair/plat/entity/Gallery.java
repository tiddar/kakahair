package com.kakahair.plat.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 图库
 * </p>
 *
 * @author Tiddar
 * @since 2018-04-07
 */

@TableName("t_gallery")
public class Gallery implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 图片路径
     */
    @TableField("imgPath")
    private String imgPath;
    /**
     * 性别
     */
    private String sex;
    /**
     * 发型名
     */
    private String hair;
    /**
     * 标题
     */
    private String title;


    public Integer getId() {
        return id;
    }

    public Gallery setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getImgPath() {
        return imgPath;
    }

    public Gallery setImgPath(String imgPath) {
        this.imgPath = imgPath;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public Gallery setSex(String sex) {
        this.sex = sex;
        return this;
    }

    public String getHair() {
        return hair;
    }

    public Gallery setHair(String hair) {
        this.hair = hair;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Gallery setTitle(String title) {
        this.title = title;
        return this;
    }

    @Override
    public String toString() {
        return "Gallery{" +
        "id=" + id +
        ", imgPath=" + imgPath +
        ", sex=" + sex +
        ", hair=" + hair +
        ", title=" + title +
        "}";
    }
}
