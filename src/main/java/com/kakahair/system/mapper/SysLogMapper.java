package com.kakahair.system.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.kakahair.system.entity.SysLog;

/**
 *
 * SysLog 表数据库控制层接口
 *
 */
public interface SysLogMapper extends BaseMapper<SysLog> {


}