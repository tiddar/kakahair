package com.kakahair.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.kakahair.system.entity.SysLog;
/**
 *
 * SysLog 表数据服务层接口
 *
 */
public interface ISysLogService extends IService<SysLog> {


}