/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100130
 Source Host           : localhost:3306
 Source Schema         : kakahair

 Target Server Type    : MySQL
 Target Server Version : 100130
 File Encoding         : 65001

 Date: 09/04/2018 20:40:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bservice
-- ----------------------------
DROP TABLE IF EXISTS `bservice`;
CREATE TABLE `bservice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '服务名(\'烫发\',\'洗剪吹\'...)',
  `price` decimal(10, 2) NOT NULL COMMENT '价钱',
  `techId` int(11) NULL DEFAULT NULL COMMENT '对应理发师Id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for evaluation
-- ----------------------------
DROP TABLE IF EXISTS `evaluation`;
CREATE TABLE `evaluation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` int(11) NOT NULL COMMENT '星级',
  `pics` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '图片//用逗号分隔',
  `add` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '其他建议',
  `orderId` int(11) NOT NULL COMMENT '对应订单Id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgPath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '图片路径',
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '性别',
  `hair` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '发型名',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 128 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '图库' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gallery
-- ----------------------------
INSERT INTO `gallery` VALUES (1, 'D:/视频/飞鸟135.jpg', '男', '发现', '123');
INSERT INTO `gallery` VALUES (2, 'D:/视频/飞鸟135.jpg', '男', '发现', '123');
INSERT INTO `gallery` VALUES (3, 'D:/视频/飞鸟135.jpg', '男', '发现', '123');
INSERT INTO `gallery` VALUES (4, 'D:/视频/飞鸟135.jpg', '男', '发现', '123');
INSERT INTO `gallery` VALUES (5, 'D:/视频/飞鸟135.jpg', '男', '发现', '123');
INSERT INTO `gallery` VALUES (6, 'D:/视频/飞鸟135.jpg', '男', '发现', '123');
INSERT INTO `gallery` VALUES (7, 'D:/视频/飞鸟135.jpg', '男', '发现', '123');
INSERT INTO `gallery` VALUES (8, 'D:/视频/飞鸟135.jpg', '男', '发现', '123');
INSERT INTO `gallery` VALUES (9, 'D:/视频/飞鸟135.jpg', '男', '发型A', '1');
INSERT INTO `gallery` VALUES (10, 'D:/视频/飞鸟135.jpg', '男', '发型A', '2');
INSERT INTO `gallery` VALUES (11, 'D:/视频/飞鸟135.jpg', '男', '发型A', '3');
INSERT INTO `gallery` VALUES (12, 'D:/视频/飞鸟135.jpg', '男', '发型A', '4');
INSERT INTO `gallery` VALUES (13, 'D:/视频/飞鸟135.jpg', '男', '发型A', '5');
INSERT INTO `gallery` VALUES (14, 'D:/视频/飞鸟135.jpg', '男', '发型A', '6');
INSERT INTO `gallery` VALUES (15, 'D:/视频/飞鸟135.jpg', '男', '发型A', '7');
INSERT INTO `gallery` VALUES (16, 'D:/视频/飞鸟135.jpg', '男', '发型A', '8');
INSERT INTO `gallery` VALUES (17, 'D:/视频/飞鸟135.jpg', '男', '发型A', '9');
INSERT INTO `gallery` VALUES (18, 'D:/视频/飞鸟135.jpg', '男', '发型A', '10');
INSERT INTO `gallery` VALUES (19, 'D:/视频/飞鸟135.jpg', '男', '发型A', '11');
INSERT INTO `gallery` VALUES (20, 'D:/视频/飞鸟135.jpg', '男', '发型A', '12');
INSERT INTO `gallery` VALUES (21, 'D:/视频/飞鸟135.jpg', '男', '发型A', '13');
INSERT INTO `gallery` VALUES (22, 'D:/视频/飞鸟135.jpg', '男', '发型A', '14');
INSERT INTO `gallery` VALUES (23, 'D:/视频/飞鸟135.jpg', '男', '发型A', '15');
INSERT INTO `gallery` VALUES (24, 'D:/视频/飞鸟135.jpg', '男', '发型A', '16');
INSERT INTO `gallery` VALUES (25, 'D:/视频/飞鸟135.jpg', '男', '发型A', '17');
INSERT INTO `gallery` VALUES (26, 'D:/视频/飞鸟135.jpg', '男', '发型A', '18');
INSERT INTO `gallery` VALUES (27, 'D:/视频/飞鸟135.jpg', '男', '发型A', '19');
INSERT INTO `gallery` VALUES (28, 'D:/视频/飞鸟135.jpg', '男', '发型A', '20');
INSERT INTO `gallery` VALUES (29, 'D:/视频/飞鸟135.jpg', '男', '发型A', '21');
INSERT INTO `gallery` VALUES (30, 'D:/视频/飞鸟135.jpg', '男', '发型A', '22');
INSERT INTO `gallery` VALUES (31, 'D:/视频/飞鸟135.jpg', '男', '发型A', '23');
INSERT INTO `gallery` VALUES (32, 'D:/视频/飞鸟135.jpg', '男', '发型A', '24');
INSERT INTO `gallery` VALUES (33, 'D:/视频/飞鸟135.jpg', '男', '发型A', '25');
INSERT INTO `gallery` VALUES (34, 'D:/视频/飞鸟135.jpg', '男', '发型A', '26');
INSERT INTO `gallery` VALUES (35, 'D:/视频/飞鸟135.jpg', '男', '发型A', '27');
INSERT INTO `gallery` VALUES (36, 'D:/视频/飞鸟135.jpg', '男', '发型A', '28');
INSERT INTO `gallery` VALUES (37, 'D:/视频/飞鸟135.jpg', '男', '发型A', '29');
INSERT INTO `gallery` VALUES (38, 'D:/视频/飞鸟135.jpg', '男', '发型A', '30');
INSERT INTO `gallery` VALUES (39, 'D:/视频/飞鸟135.jpg', '男', '发型A', '31');
INSERT INTO `gallery` VALUES (40, 'D:/视频/飞鸟135.jpg', '男', '发型A', '32');
INSERT INTO `gallery` VALUES (41, 'D:/视频/飞鸟135.jpg', '男', '发型A', '33');
INSERT INTO `gallery` VALUES (42, 'D:/视频/飞鸟135.jpg', '男', '发型A', '34');
INSERT INTO `gallery` VALUES (43, 'D:/视频/飞鸟135.jpg', '男', '发型A', '35');
INSERT INTO `gallery` VALUES (44, 'D:/视频/飞鸟135.jpg', '男', '发型A', '36');
INSERT INTO `gallery` VALUES (45, 'D:/视频/飞鸟135.jpg', '男', '发型A', '37');
INSERT INTO `gallery` VALUES (46, 'D:/视频/飞鸟135.jpg', '男', '发型A', '38');
INSERT INTO `gallery` VALUES (47, 'D:/视频/飞鸟135.jpg', '男', '发型A', '39');
INSERT INTO `gallery` VALUES (48, 'D:/视频/飞鸟135.jpg', '男', '发型A', '40');
INSERT INTO `gallery` VALUES (49, 'D:/视频/飞鸟135.jpg', '男', '发型A', '41');
INSERT INTO `gallery` VALUES (50, 'D:/视频/飞鸟135.jpg', '男', '发型A', '42');
INSERT INTO `gallery` VALUES (51, 'D:/视频/飞鸟135.jpg', '男', '发型A', '43');
INSERT INTO `gallery` VALUES (52, 'D:/视频/飞鸟135.jpg', '男', '发型A', '44');
INSERT INTO `gallery` VALUES (53, 'D:/视频/飞鸟135.jpg', '男', '发型A', '45');
INSERT INTO `gallery` VALUES (54, 'D:/视频/飞鸟135.jpg', '男', '发型A', '46');
INSERT INTO `gallery` VALUES (55, 'D:/视频/飞鸟135.jpg', '男', '发型A', '47');
INSERT INTO `gallery` VALUES (56, 'D:/视频/飞鸟135.jpg', '男', '发型A', '48');
INSERT INTO `gallery` VALUES (57, 'D:/视频/飞鸟135.jpg', '男', '发型A', '49');
INSERT INTO `gallery` VALUES (58, 'D:/视频/飞鸟135.jpg', '男', '发型A', '50');
INSERT INTO `gallery` VALUES (59, 'D:/视频/飞鸟135.jpg', '男', '发型A', '51');
INSERT INTO `gallery` VALUES (60, 'D:/视频/飞鸟135.jpg', '男', '发型A', '52');
INSERT INTO `gallery` VALUES (61, 'D:/视频/飞鸟135.jpg', '男', '发型A', '53');
INSERT INTO `gallery` VALUES (62, 'D:/视频/飞鸟135.jpg', '男', '发型A', '54');
INSERT INTO `gallery` VALUES (63, 'D:/视频/飞鸟135.jpg', '男', '发型A', '55');
INSERT INTO `gallery` VALUES (64, 'D:/视频/飞鸟135.jpg', '男', '发型A', '56');
INSERT INTO `gallery` VALUES (65, 'D:/视频/飞鸟135.jpg', '男', '发型A', '57');
INSERT INTO `gallery` VALUES (66, 'D:/视频/飞鸟135.jpg', '男', '发型A', '58');
INSERT INTO `gallery` VALUES (67, 'D:/视频/飞鸟135.jpg', '男', '发型A', '59');
INSERT INTO `gallery` VALUES (68, 'D:/视频/飞鸟135.jpg', '男', '发型A', '60');
INSERT INTO `gallery` VALUES (69, 'D:/视频/飞鸟135.jpg', '男', '发型A', '61');
INSERT INTO `gallery` VALUES (70, 'D:/视频/飞鸟135.jpg', '男', '发型A', '62');
INSERT INTO `gallery` VALUES (71, 'D:/视频/飞鸟135.jpg', '男', '发型A', '63');
INSERT INTO `gallery` VALUES (72, 'D:/视频/飞鸟135.jpg', '男', '发型A', '64');
INSERT INTO `gallery` VALUES (73, 'D:/视频/飞鸟135.jpg', '男', '发型A', '65');
INSERT INTO `gallery` VALUES (74, 'D:/视频/飞鸟135.jpg', '男', '发型A', '66');
INSERT INTO `gallery` VALUES (75, 'D:/视频/飞鸟135.jpg', '男', '发型A', '67');
INSERT INTO `gallery` VALUES (76, 'D:/视频/飞鸟135.jpg', '男', '发型A', '68');
INSERT INTO `gallery` VALUES (77, 'D:/视频/飞鸟135.jpg', '男', '发型A', '69');
INSERT INTO `gallery` VALUES (78, 'D:/视频/飞鸟135.jpg', '男', '发型A', '70');
INSERT INTO `gallery` VALUES (79, 'D:/视频/飞鸟135.jpg', '男', '发型A', '71');
INSERT INTO `gallery` VALUES (80, 'D:/视频/飞鸟135.jpg', '男', '发型A', '72');
INSERT INTO `gallery` VALUES (81, 'D:/视频/飞鸟135.jpg', '男', '发型A', '73');
INSERT INTO `gallery` VALUES (82, 'D:/视频/飞鸟135.jpg', '男', '发型A', '74');
INSERT INTO `gallery` VALUES (83, 'D:/视频/飞鸟135.jpg', '男', '发型A', '75');
INSERT INTO `gallery` VALUES (84, 'D:/视频/飞鸟135.jpg', '男', '发型A', '76');
INSERT INTO `gallery` VALUES (85, 'D:/视频/飞鸟135.jpg', '男', '发型A', '77');
INSERT INTO `gallery` VALUES (86, 'D:/视频/飞鸟135.jpg', '男', '发型A', '78');
INSERT INTO `gallery` VALUES (87, 'D:/视频/飞鸟135.jpg', '男', '发型A', '79');
INSERT INTO `gallery` VALUES (88, 'D:/视频/飞鸟135.jpg', '男', '发型A', '80');
INSERT INTO `gallery` VALUES (89, 'D:/视频/飞鸟135.jpg', '男', '发型A', '81');
INSERT INTO `gallery` VALUES (90, 'D:/视频/飞鸟135.jpg', '男', '发型A', '82');
INSERT INTO `gallery` VALUES (91, 'D:/视频/飞鸟135.jpg', '男', '发型A', '83');
INSERT INTO `gallery` VALUES (92, 'D:/视频/飞鸟135.jpg', '男', '发型A', '84');
INSERT INTO `gallery` VALUES (93, 'D:/视频/飞鸟135.jpg', '男', '发型A', '85');
INSERT INTO `gallery` VALUES (94, 'D:/视频/飞鸟135.jpg', '男', '发型A', '86');
INSERT INTO `gallery` VALUES (95, 'D:/视频/飞鸟135.jpg', '男', '发型A', '87');
INSERT INTO `gallery` VALUES (96, 'D:/视频/飞鸟135.jpg', '男', '发型A', '88');
INSERT INTO `gallery` VALUES (97, 'D:/视频/飞鸟135.jpg', '男', '发型A', '89');
INSERT INTO `gallery` VALUES (98, 'D:/视频/飞鸟135.jpg', '男', '发型A', '90');
INSERT INTO `gallery` VALUES (99, 'D:/视频/飞鸟135.jpg', '男', '发型A', '91');
INSERT INTO `gallery` VALUES (100, 'D:/视频/飞鸟135.jpg', '男', '发型A', '92');
INSERT INTO `gallery` VALUES (101, 'D:/视频/飞鸟135.jpg', '男', '发型A', '93');
INSERT INTO `gallery` VALUES (102, 'D:/视频/飞鸟135.jpg', '男', '发型A', '94');
INSERT INTO `gallery` VALUES (103, 'D:/视频/飞鸟135.jpg', '男', '发型A', '95');
INSERT INTO `gallery` VALUES (104, 'D:/视频/飞鸟135.jpg', '男', '发型A', '96');
INSERT INTO `gallery` VALUES (105, 'D:/视频/飞鸟135.jpg', '男', '发型A', '97');
INSERT INTO `gallery` VALUES (106, 'D:/视频/飞鸟135.jpg', '男', '发型A', '98');
INSERT INTO `gallery` VALUES (107, 'D:/视频/飞鸟135.jpg', '男', '发型A', '99');
INSERT INTO `gallery` VALUES (108, 'D:/视频/飞鸟135.jpg', '男', '发型A', '100');
INSERT INTO `gallery` VALUES (109, 'D:/视频/飞鸟135.jpg', '男', '发型A', '101');
INSERT INTO `gallery` VALUES (110, 'D:/视频/飞鸟135.jpg', '男', '发型A', '102');
INSERT INTO `gallery` VALUES (111, 'D:/视频/飞鸟135.jpg', '男', '发型A', '103');
INSERT INTO `gallery` VALUES (112, 'D:/视频/飞鸟135.jpg', '男', '发型A', '104');
INSERT INTO `gallery` VALUES (113, 'D:/视频/飞鸟135.jpg', '男', '发型A', '105');
INSERT INTO `gallery` VALUES (114, 'D:/视频/飞鸟135.jpg', '男', '发型A', '106');
INSERT INTO `gallery` VALUES (115, 'D:/视频/飞鸟135.jpg', '男', '发型A', '107');
INSERT INTO `gallery` VALUES (116, 'D:/视频/飞鸟135.jpg', '男', '发型A', '108');
INSERT INTO `gallery` VALUES (117, 'D:/视频/飞鸟135.jpg', '男', '发型A', '109');
INSERT INTO `gallery` VALUES (118, 'D:/视频/飞鸟135.jpg', '男', '发型A', '110');
INSERT INTO `gallery` VALUES (119, 'D:/视频/飞鸟135.jpg', '男', '发型A', '111');
INSERT INTO `gallery` VALUES (120, 'D:/视频/飞鸟135.jpg', '男', '发型A', '112');
INSERT INTO `gallery` VALUES (121, 'D:/视频/飞鸟135.jpg', '男', '发型A', '113');
INSERT INTO `gallery` VALUES (122, 'D:/视频/飞鸟135.jpg', '男', '发型A', '114');
INSERT INTO `gallery` VALUES (123, 'D:/视频/飞鸟135.jpg', '男', '发型A', '115');
INSERT INTO `gallery` VALUES (124, 'D:/视频/飞鸟135.jpg', '男', '发型A', '116');
INSERT INTO `gallery` VALUES (125, 'D:/视频/飞鸟135.jpg', '男', '发型A', '117');
INSERT INTO `gallery` VALUES (126, 'D:/视频/飞鸟135.jpg', '男', '发型A', '118');
INSERT INTO `gallery` VALUES (127, 'D:/视频/飞鸟135.jpg', '男', '发型A', '119');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evaluate` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `used` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payType` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payTime` datetime(0) NULL,
  `allPrice` decimal(10, 2) NOT NULL,
  `appointDate` datetime(0) NULL,
  `appointTime` datetime(0) NULL,
  `userId` int(11) NOT NULL,
  `techId` int(11) NOT NULL,
  `shopId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '订单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for shop
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '商铺名',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '商铺店面照片',
  `minPrice` decimal(10, 2) NOT NULL COMMENT '最低价',
  `avePrice` decimal(10, 2) NOT NULL COMMENT '平均消费',
  `local` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '详细位置/***第几楼，第几层',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '预约电话',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '模糊位置',
  `star` decimal(65, 0) NOT NULL COMMENT ' 平均星级',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '所在市',
  `area` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '所在区',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '用户ID',
  `content` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '日志内容',
  `operation` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户操作',
  `crTime` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 980127841381879809 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (423827573609332736, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=abc&loginName=abc&,[IP]:10.10.10.10', '登录', '2016-05-09 21:55:10');
INSERT INTO `sys_log` VALUES (423827889272651776, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=abc&loginName=admin&,[IP]:10.10.10.10', '登录', '2016-05-09 21:56:23');
INSERT INTO `sys_log` VALUES (423828945075437568, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=acbbb&repassword=acbbb&loginName=acbbb&,[IP]:10.10.10.10', '登录', '2016-05-09 22:00:37');
INSERT INTO `sys_log` VALUES (423828960116211712, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:insertSelective,[参数]:password=acbbb&repassword=acbbb&loginName=acbbb&,[IP]:10.10.10.10', '添加用户', '2016-05-09 22:00:40');
INSERT INTO `sys_log` VALUES (423829494923526144, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=abc&loginName=admin&,[IP]:10.10.10.10', '登录', '2016-05-09 22:02:48');
INSERT INTO `sys_log` VALUES (423829536409387008, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=abc&loginName=admin&,[IP]:10.10.10.10', '登录', '2016-05-09 22:02:58');
INSERT INTO `sys_log` VALUES (423829555678019584, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=123&loginName=admin&,[IP]:10.10.10.10', '登录', '2016-05-09 22:03:02');
INSERT INTO `sys_log` VALUES (423829597029662720, 1, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:hasDeleteUser,[参数]:null,[IP]:10.10.10.10', '删除用户', '2016-05-09 22:03:12');
INSERT INTO `sys_log` VALUES (423829597029662721, 1, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:hasDeleteUser,[参数]:null,[IP]:10.10.10.10', '删除用户', '2016-05-09 22:03:12');
INSERT INTO `sys_log` VALUES (424204895713755136, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=123&,[IP]:10.10.10.10', '登录', '2016-05-10 22:54:30');
INSERT INTO `sys_log` VALUES (424204946615828480, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=abc&loginName=admin&,[IP]:10.10.10.10', '登录', '2016-05-10 22:54:42');
INSERT INTO `sys_log` VALUES (424204971169284096, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=123&loginName=admin&,[IP]:10.10.10.10', '登录', '2016-05-10 22:54:48');
INSERT INTO `sys_log` VALUES (424212917953495040, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=123&,[IP]:10.10.10.10', '登录', '2016-05-10 23:26:23');
INSERT INTO `sys_log` VALUES (424213509627183104, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=123&,[IP]:10.10.10.10', '登录', '2016-05-10 23:28:44');
INSERT INTO `sys_log` VALUES (424213551029157888, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=123&loginName=admin&,[IP]:10.10.10.10', '登录', '2016-05-10 23:28:54');
INSERT INTO `sys_log` VALUES (424538597211766784, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=abc&loginName=abc&,[IP]:10.10.10.10', '登录', '2016-05-11 21:00:31');
INSERT INTO `sys_log` VALUES (424538650328432640, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=abc&loginName=admin&,[IP]:10.10.10.10', '登录', '2016-05-11 21:00:44');
INSERT INTO `sys_log` VALUES (424538669534150656, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=abc&loginName=admin&,[IP]:10.10.10.10', '登录', '2016-05-11 21:00:48');
INSERT INTO `sys_log` VALUES (424538690912518144, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=123&loginName=admin&,[IP]:10.10.10.10', '登录', '2016-05-11 21:00:53');
INSERT INTO `sys_log` VALUES (424538709661057024, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=123&,[IP]:10.10.10.10', '登录', '2016-05-11 21:00:58');
INSERT INTO `sys_log` VALUES (424538725716852736, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:password=123&,[IP]:10.10.10.10', '登录', '2016-05-11 21:01:02');
INSERT INTO `sys_log` VALUES (980127308516528128, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:loginName=HRX&password=123&ctoken=b5853c2d4921489fa35a0b74b8b32441&captcha=a76s&rememberMe=on&,[IP]:192.168.31.223', '登录', '2018-04-01 00:58:54');
INSERT INTO `sys_log` VALUES (980127504851898368, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:loginName=HRX&password=123&ctoken=346c58d8856746dd877fb67b1973c7d0&captcha=b7wm&rememberMe=on&,[IP]:192.168.31.223', '登录', '2018-04-01 00:59:41');
INSERT INTO `sys_log` VALUES (980127538049814528, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:loginName=admi&password=123&ctoken=47187f0f957e43b6940e47fc06bfbce0&captcha=sees&rememberMe=on&,[IP]:192.168.31.223', '登录', '2018-04-01 00:59:49');
INSERT INTO `sys_log` VALUES (980127732078317568, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:loginName=admin&password=123&ctoken=163cc71e34eb4645b542afe1a47bb07c&captcha=m3gp&rememberMe=on&,[IP]:192.168.31.223', '登录', '2018-04-01 01:00:35');
INSERT INTO `sys_log` VALUES (980127773773893632, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:loginName=test&password=123&ctoken=72548d5eff004150a194640da5d1889a&captcha=7wxg&rememberMe=on&,[IP]:192.168.31.223', '登录', '2018-04-01 01:00:45');
INSERT INTO `sys_log` VALUES (980127841381879808, NULL, '[类名]:com.baomidou.springwind.service.impl.UserServiceImpl,[方法]:selectByLoginName,[参数]:loginName=admin&password=123&ctoken=a6183795bfd14b00bc5d883472d0de75&captcha=wopo&rememberMe=on&,[IP]:192.168.31.223', '登录', '2018-04-01 01:01:01');

-- ----------------------------
-- Table structure for technician
-- ----------------------------
DROP TABLE IF EXISTS `technician`;
CREATE TABLE `technician`  (
  `id` int(11) NOT NULL,
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '理发师照片',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '理发师姓名',
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '理发师职位',
  `workAge` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '理发师工作时长',
  `score` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '理发师历史评分,使用\',\'隔开',
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '理发师简介',
  `restTime` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '每周休息日，使用\',\'隔开',
  `shopId` int(11) NULL DEFAULT NULL COMMENT '对应理发店Id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '理发师表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL,
  `userLogin` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Procedure structure for galleryInit
-- ----------------------------
DROP PROCEDURE IF EXISTS `galleryInit`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `galleryInit`()
BEGIN 

DECLARE i INT DEFAULT 1;

WHILE i<128
DO 
UPDATE gallery SET imgPath = CONCAT('D:/视频/飞鸟',i+8,'.jpg');
SET i = i+1;
END WHILE ; 
commit; 

END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
